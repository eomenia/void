# Void Client Setup

[TOC]

## Install Instructions
1. Install OS, by [script](https://github.com/Le0xFF/VoidLinuxInstaller) or [manually](https://docs.voidlinux.org/installation/index.html). 
    - Use the [Void docs](https://docs.voidlinux.org) as reference during the process.
2. Download [personal scripts](https://gitlab.com/eomenia/void/-/tree/main/scripts) and run:
    - `bash pkg-installer.sh`
3. Final walkthrough of the [Void docs](https://docs.voidlinux.org/installation/index.html) for any essential post-install configuration. 
4. Reboot and evaluate system functionality. Is the following working correctly?
    - Wayland backend
    - Locale, language & time
    - Key bindings
    - Screen & font rendering
    - Necessary services
    - Networking & VPN
    - Audio
    - Group membership & permissions
    - Sleep & screen idling
    - Firmware & graphics drivers 
5. Configure basic settings for:
    - Shell
    - Prompt
    - Terminal
    - Wayland
    - Window Manager
    - 

## To do
- [ ] **Document next Void install fully**
    - [ ] Troubleshooting logs
    - [ ] Links, tips, etc.
- [ ] Install Emacs (Doom?)
    - [ ] Finalize local Emacs setup
    - [ ] Install Logseq on phone
        - [ ] Set up Syncthing and integrate applications
    - [ ] **Get PKM routine going**
- [ ] Proper GIT setup (locally or on Gitlab)
    - [ ] Dot-files & misc.
    - [ ] Config/settings backup routine
- [ ] Proper local system backup routine
- [ ] **Implement use of pw man & other utilities across all systems**
- [ ] Implement custom install/config scripts for future installations
