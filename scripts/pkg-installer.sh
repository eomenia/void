#!/bin/bash

# xbps-install list of necessary (or useful) Void packages 
# Add additional packages to 'package.list' 
# 'package.list' is expected to be in the script dir

# Strip out comments and empty lines from package.list
packageList=$(sed -E '/^[[:blank:]]*(#|$)/d; s/#.*//' package.list)

echo "Updating system..."
read -p "Press Enter to continue:"

sudo xbps-install Suy

echo "Installing packages from list..."
read -p "Press Enter to continue:"

sudo xbps-install -Suy ${packageList[*]}

echo "Package install script complete"
sleep 1
echo "<3"
sleep 1
